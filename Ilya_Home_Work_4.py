from flask import Flask, render_template
import requests
from time import time

app = Flask(__name__)


class Timer:

    def __init__(self, name, flags='a'):
        self.obj = open(name, flags)

    def __enter__(self):
        self.start_time = time()
        return self.obj

    def __exit__(self, exc_type, exc_val, exc_tb):
        done = round(((time() - self.start_time) * 1000), 2)
        status_text = "Успешный" if not exc_type else exc_val
        self.obj.write(f"Статус программы: {status_text}\nЗавершено за {done} миллисекунд\n{'_'*30}\n")
        self.obj.close()
        return True


@app.route('/')
def hello_world():
    with Timer("logs.txt"):
        return '<h2>My Home Work 4!</h2>'


@app.route('/astro/list')
def astro_list():
    with Timer("logs.txt"):
        astro_list = requests.get('http://api.open-notify.org/astros.json')
        ppl = astro_list.json()['people']
        return render_template('index.html', users=ppl)


@app.route('/astro/craft/<craft_name>')
def craft_list(craft_name):
    with Timer("logs.txt"):
        astro_list = requests.get('http://api.open-notify.org/astros.json')
        ppl = astro_list.json()['people']
        for i in ppl:
            if craft_name == i['craft']:
                return render_template('index.html', craft_name=craft_name, ppl=ppl)
            else:
                pass


@app.route('/file/<file_name>')
def open_file(file_name):
    with Timer("logs.txt"):
        with open(file_name) as file_f:
            return render_template('index.html', file_name=file_name, file_body=file_f.readlines())


@app.route('/logs')
def open_logs():
    with Timer("logs.txt"):
        with open('logs.txt') as logs_f:
            return render_template('index.html', logs_body=logs_f.readlines())


if __name__ == '__main__':
    app.run()
