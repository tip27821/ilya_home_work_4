
Фанаты Half-Life 2 готовятся устроить флешмоб в этом месяце: они вновь зайдут в игру и установят в ней новый пиковый онлайн в Steam

Флэшмоб начнётся 14 августа в 18:00 по московскому времени. Чтобы принять в нём участие, нужно будет зайти в обычную вторую часть.

Участникам акции также нужно будет установить публичный статус аккаунту в Steam, а также запустить клиент Steam и саму игру с подключением к интернету.

Аналогичный флешмоб уже проходил в феврале 2012 года. Тогда пиковый онлайн составил свыше 13 тысяч